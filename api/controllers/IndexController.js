/**
 * Index Controller
 *
 * 
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

// var RateLimiter = require('limiter').RateLimiter;
var request     = require('request');
// var limiter     = new RateLimiter(global.limitVal, "hour", true); // fire CB immediately


global.limitVal = 11;
module.exports = {
	uptime: async (req, res, next) => {
        id = req.session.userId;
		if (id === undefined || id === '') {
			return res.redirect("/");
		}
		var uptime = process.uptime();
		return res.json({ uptime: uptime });
    },

    change : async (req, res, next) => {
        if (req.session.userId === "admin") {
            var number = req.param('no_req');
            await Users.update({}).set({ request: number });
            return res.redirect('/panel');
        }
        return res.redirect('/');
    },

    panel: async (req, res, next) => {
        if (req.session.userId === "admin") {
            return res.view('pages/panel');
        }
        return res.redirect('/');
    },
    
    adminLogin: async (req, res, next) => {
        var username = req.param('user');
        var password = req.param('password');
        if (username === 'admin' && password === 'admin') {
            req.session.userId = "admin";
            return res.redirect('/panel');
        } 
        return res.redirect('/');
    },
	rangeUptime: async (req, res, next) => {
        id = req.session.userId;
        if (id === undefined || id === '') {
            return res.redirect("/");
        }
		var lineReader = require("readline").createInterface({
			input: require("fs").createReadStream("access.log")
		});

		var startDate = Date.parse(req.param("start"));
		var endDate = Date.parse(req.param("end"));
		sails.log(startDate);
		sails.log(endDate);

		var resArr = [];
		await lineReader
			.on("line", line => {
				// sails.log('Line from file:', line);
				var pattern = /(\d{2}([\/])\w{3}([\/])\d{4}:\d{2}:\d{2}:\d{2})/;
				var matchedDate = pattern.exec(line);
				matchedDate =
					matchedDate[0].split(":")[0].replace("/", " ") +
					" " +
					matchedDate[0]
						.split(":")
						.slice(1)
                        .join(":");
                        
				var timeInSec = Date.parse(matchedDate);


				if (timeInSec >= startDate && timeInSec <= endDate) {
                    resArr.push(line);
				}
			})
			.on("close", function() {
				return res.json({ uptime: resArr });
			});
	},

	dashboard: async (req, res, next) => {
        id = req.session.userId;
        if (id === undefined || id === '') {
            return res.redirect("/");
        }
        var user = await Users.find({ id: id });
        return res.view('pages/dashboard', { user : user[0] });
	},

	createUser: async function(req, res, next) {
        var email       = req.param("email");
		var id          = req.param("id");
		var image_url   = req.param("image_url");
		var name        = req.param("name");
		var totalUsers  = await Users.count({});
		var countID     = totalUsers + 1;
		await Users.findOrCreate(
			{ email: email },
			{
				id: countID,
				profile_id: id,
				name: name,
				email: email,
                image_url: image_url,
                is_admin: "false",
                request : "10"
			}
		).exec(async (err, user, wasCreated) => {
			if (err) {
				sails.log("error is ----------> " + err);
				return res.serverError(err);
			}

			if (wasCreated) {
				sails.log("Created a new user: " + user.id);
			} else {
				sails.log("Found existing user: " + user.id);
			}
			req.session.loggedIn = true;
			req.session.userId = user.id;
			return res.json({ success: true, user_id: user.id });
		});
	},

	reverseGeocoding: async (req, res, next) => {
        id = req.session.userId;
        if (id === undefined || id === '') {
            return res.redirect("/");
        }
        var user = await Users.find({ id : id });

        var requestLeft = parseInt(user[0].request);
        sails.log(requestLeft);
        if (requestLeft > 0 ) {
            await Users.update({ id: id }).set({
                request: requestLeft - 1
            });
            var coordinates = req.param('latlng');
            // var coordinates = '40.714224,-73.961452';
            var latitude = coordinates.split(",")[0];
            var longitude = coordinates.split(",")[1];
            request(
                `https://maps.googleapis.com/maps/api/geocode/json?latlng=${latitude},${longitude}&key=AIzaSyAiqOpKwSqA8rIbFpIzMNA7mX4u3H_9mUw`,
                function (error, response, body) {
                    if (error) {
                        return res.serverError();
                    }
                    return res.json({ location: body });
                }
            );
        } else {
            return res.send("429 Too Many Requests - your account is being rate limited");
        }

    },

    signOut: async function (req, res, next) {
        req.session.loggedIn = false;
        req.session.userId = "";
        return res.json({ success: true });
    }

};