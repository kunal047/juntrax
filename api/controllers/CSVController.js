
/**
 * Index Controller
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    index : async (req, res, next) => {
        return res.view("pages/index");
    }
};