module.exports = {
	tableName: "users",
	attributes: {
		id: {
			type: "number",
			columnName: "id",
			required: true
		},
		profile_id: {
			type: "string",
			required: true
		},
		name: {
			type: "string",
			required: true
		},
		email: {
			type: "string",
			allowNull: true
		},
		image_url: {
			type: "string",
			allowNull: true
		},
		is_admin: {
			type: "string",
			allowNull: true
		},
		request: {
			type: "string",
			allowNull: true
		}
	}
};