# juntrax

a [Sails v1](https://sailsjs.com) application


### Links

+ [Get started](https://sailsjs.com/get-started)
+ [Sails framework documentation](https://sailsjs.com/documentation)
+ [Version notes / upgrading](https://sailsjs.com/documentation/upgrading)
+ [Deployment tips](https://sailsjs.com/documentation/concepts/deployment)
+ [Community support options](https://sailsjs.com/support)
+ [Professional / enterprise options](https://sailsjs.com/enterprise)


### Version info

This app was originally generated on Wed Dec 26 2018 20:19:35 GMT+0530 (India Standard Time) using Sails v1.0.2.

<!-- Internally, Sails used [`sails-generate@1.15.26`](https://github.com/balderdashy/sails-generate/tree/v1.15.26/lib/core-generators/new). -->

+ Fetch Uptime - [URL](http://test.kunalchauhan.in/uptime)
+ Get the log range using the DD-mmm-YY format. For ex. test.kunalchauhan.in/28-Dec-2018/29-Dec-2018 or with time test.kunalchauhan.in/28-Dec-2018 00:00:00/29-Dec-2018 12:00:00. Click on the below link to test
+ [LINK](http://test.kunalchauhan.in/range/28-Dec-2018/29-Dec-2018)
+ Access Log is not being updated.
+ [For reverse geocoding](http://test.kunalchauhan.in/location?latlng=40.714224,-73.961452)
+ [For the front end solution](http://test.kunalchauhan.in/csv)


<!--
Note:  Generators are usually run using the globally-installed `sails` CLI (command-line interface).  This CLI version is _environment-specific_ rather than app-specific, thus over time, as a project's dependencies are upgraded or the project is worked on by different developers on different computers using different versions of Node.js, the Sails dependency in its package.json file may differ from the globally-installed Sails CLI release it was originally generated with.  (Be sure to always check out the relevant [upgrading guides](https://sailsjs.com/upgrading) before upgrading the version of Sails used by your app.  If you're stuck, [get help here](https://sailsjs.com/support).)
-->

