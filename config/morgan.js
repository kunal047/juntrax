module.exports.morgan = {
  format : "[:date[clf]] :method :url :status", 
  stream : require("fs").createWriteStream("access.log", { flags: "a" })

};